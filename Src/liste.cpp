#include "liste.hpp"

#include <iostream>
#include <cassert>
#include <cstdio>

Liste::Liste() {
  /* votre code ici */
	_tete = nullptr;
}

Liste::Liste(const Liste& autre) {
  /* votre code ici */
	if(autre._tete == nullptr) {
		_tete = nullptr;
	} else {
		Cellule* c = new Cellule;
		_tete = c;
		c->valeur = autre._tete->valeur;
		for(Cellule* o = autre._tete->next; o; o = o->next) {
			c->next = new Cellule;
			c = c->next;
			c->valeur = o->valeur;
		}
	}
}

Liste& Liste::operator=(const Liste& autre) {
  /* votre code ici */
	if(_tete) {
		for(Cellule* c = _tete; c;) {
			Cellule* n = c->next;
			delete c;
			c = n;
		}
	}
	if(autre._tete == nullptr) {
		_tete = nullptr;
	} else {
		Cellule* c = new Cellule;
		_tete = c;
		c->valeur = autre._tete->valeur;
		for(Cellule* o = autre._tete->next; o; o = o->next) {
			c->next = new Cellule;
			c = c->next;
			c->valeur = o->valeur;
		}
	}
  return *this ;
}

Liste::~Liste() {
  /* votre code ici */
	if(_tete) {
		for(Cellule* c = _tete; c;) {
			Cellule* n = c->next;
			delete c;
			c = n;
		}
	}
}

void Liste::ajouter_en_tete(int valeur) {
  /* votre code ici */
	Cellule* c = new Cellule;
	c->next = _tete;
	c->valeur = valeur;
	_tete = c;
}

void Liste::ajouter_en_queue(int valeur) {
  /* votre code ici */
	Cellule* c = new Cellule;
	c->valeur = valeur;
	c->next = nullptr;
	queue()->next = c;
}

void Liste::supprimer_en_tete() {
  /* votre code ici */
	assert(_tete);
	Cellule* c = _tete;
	_tete = c->next;
	delete c;
}

Cellule* Liste::tete() {
  /* votre code ici */
	return _tete;
}

const Cellule* Liste::tete() const {
  /* votre code ici */
	return _tete;
}

Cellule* Liste::queue() {
  /* votre code ici */
	Cellule* c = _tete; 
	for(;c && c->next; c = c->next);
	return c;
}

const Cellule* Liste::queue() const {
  /* votre code ici */
	Cellule* c = _tete; 
	for(;c && c->next; c = c->next);
	return c;
}

int Liste::taille() const {
  /* votre code ici */
	int i = 0;
	for(Cellule* c = _tete; c; c = c->next) i++;
	return i ;
}

Cellule* Liste::recherche(int valeur) {
  /* votre code ici */
	for(Cellule* c = _tete; c; c = c->next) {
		if(c->valeur == valeur) return c;
	}
	return nullptr ;
}

const Cellule* Liste::recherche(int valeur) const {
  /* votre code ici */
	for(Cellule* c = _tete; c; c = c->next) {
		if(c->valeur == valeur) return c;
	}
	return nullptr ;
}

void Liste::afficher() const {
  /* votre code ici */
	if(!_tete) {
		printf("[ ]\n");
		return;
	}
	printf("[ ");
	for(Cellule* c = _tete; c; c = c->next) printf("%d ", c->valeur);
	printf("]\n");
}
